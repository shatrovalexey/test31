<?php
	/**
	* Задача https://www.evernote.com/shard/s487/sh/daa815e8-1816-4b82-8764-a6e262fe5780/8e0792de064951a3
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @subpackage Application
	*/

	namespace Application ;

	class Application {
		/**
		* @var $_dbh PDO - подключение к СУБД
		* @var $_dbname string - имя БД
		* @var $_tables array - хэш таблиц в БД: "имя таблицы" => "комментарий таблицы"
		* @var $_fields array - хэш полей в таблицах БД: "комментарий поля" => "путь к полю в БД"
		* @var $_props array - хэш хинтов запросов для полей из таблицы `users_about`
		*/

		protected $_dbh ;
		protected $_dbname ;
		protected $_tables ;
		protected $_fields ;
		protected $_props ;

		/**
		* Конструктор класса
		* @param $dbh \PDO - подключение к СУБД
		*/
		public function __construct( $dbh ) {
			$this->_dbh = $dbh ;
		}

		/**
		* Инициализация класса: загрузка таблиц, полей и хинтов к полям
		* @param $dbh - подключение к СУБД
		* @return $this \Application
		*/
		public function prepare( ) {
			return $this->_dbname( )->_tables( )->_fields( )->_props( ) ;
		}

		/**
		* Загрузка имени текущей БД
		* @return $this \Application
		*/
		protected function _dbname( ) {
			$this->_dbname = $this->_dbh->query( '
SELECT
	database( ) AS `dbname` ;
			' )->fetchColumn( 0 ) ;

			return $this ;
		}

		/**
		* Загрузка информации о таблицах текущей БД
		* @return $this \Application
		*/
		protected function _tables( ) {
			$sth = $this->_dbh->prepare( '
SELECT
	`t1`.`table_name` AS `name` ,
	`t1`.`table_comment` AS `comment`
FROM
	`information_schema`.`tables` AS `t1`
WHERE
	( `t1`.`table_schema` = :dbname ) ;
			' ) ;
			$sth->bindParam( ':dbname' , $this->_dbname , \PDO::PARAM_STR ) ;
			$sth->execute( ) ;
			while ( list( $name , $comment ) = $sth->fetch( \PDO::FETCH_NUM ) ) {
				$this->_tables[ $name ] = $comment ;
			}
			$sth->closeCursor( ) ;

			return $this ;
		}

		/**
		* Загрузка информации о полях текущей БД в хэш: "комментарий поля" => "имя поля"
		* @return $this \Application
		*/
		protected function _fields( ) {
			foreach ( $this->_tables as $table_name => $comment ) {
				$sth = $this->_dbh->prepare( "
SELECT SQL_SMALL_RESULT SQL_CACHE
	lower( concat( '`' , `c1`.`table_name` , '`.`' , `c1`.`column_name` , '`' ) ) AS `name` ,
	lower( concat_ws( ' ' , `c1`.`column_comment` , `t1`.`table_comment` ) ) AS `comment`
FROM
	`information_schema`.`columns` AS `c1`

	INNER JOIN `information_schema`.`tables` AS `t1` ON
	( `c1`.`table_schema` = `t1`.`table_schema` ) AND
	( `c1`.`table_name` = `t1`.`table_name` )
WHERE
	( `c1`.`table_schema` = :dbname ) AND
	( `c1`.`table_name` = :table_name ) ;
				" ) ;
				$sth->bindParam( ':dbname' , $this->_dbname , \PDO::PARAM_STR ) ;
				$sth->bindParam( ':table_name' , $table_name , \PDO::PARAM_STR ) ;
				$sth->execute( ) ;
				while ( list( $field_name , $field_comment ) = $sth->fetch( \PDO::FETCH_NUM ) ) {
					$this->_fields[ $field_comment ] = $field_name ;
				}
				$sth->closeCursor( ) ;
			}

			return $this ;
		}

		/**
		* Загрузка хинтов для полей для поиска по таблице `users_about`: "доп.имя в запросе" => "сложное значение"
		* @return $this \Application
		*/
		protected function _props( ) {
			$this->_fields[ 'ID' ] = '`u1`.`id`' ;

			$table_name = 'users_about' ;
			$table_name_r = $this->_tables[ $table_name ] ;

			$sth = $this->_dbh->prepare( '
SELECT SQL_SMALL_RESULT SQL_CACHE DISTINCT
	`ua1`.`item`
FROM
	`' . $table_name . '` AS `ua1` ;
			' ) ;
			$sth->execute( ) ;
			foreach ( $sth->fetchAll( \PDO::FETCH_COLUMN ) as $field ) {
				$this->_props[ $field ] = '(
	SELECT
		`t1`.`value`
	FROM
		`' . $table_name . '` AS `t1`
	WHERE
		( `users`.`id` = `t1`.`user` ) AND
		( `t1`.`item` = ' . $this->_dbh->quote( $field ) . ' )
	LIMIT 1
				)' ;
			}
			$sth->closeCursor( ) ;

			return $this ;
		}

		/**
		* Выполняет входной "SQL" (локальный формат)
		* @param $query string - строка с запросом в с локальным SQL-форматом, как описано в исходной задаче:
		* Должна быть возможность составлять такие условия поиска как:
		* ((ID = 1000) ИЛИ (Страна != Россия))
		* ((Страна = Россия) И (Состояние пользователя != active) И (Граватар = Нет))
		* ((((Страна != Россия) ИЛИ (Состояние пользователя = active)) И (E-Mail = user@domain.com)) ИЛИ (Граватар = Есть))
		* @return $result array - |матрица|=2 с данными о пользователе
		*/
		public function execute( $query ) {
			list( $sql_where , $sql_args ) = $this->sql_where( $query ) ;
			$sql = '
SELECT
	`users`.`id` ,
	`users`.`email` ,
	`users`.`role` ,
	`users`.`reg_date`
FROM
	`users`
			' ;

			if ( ! empty( $sql_where ) ) {
				$sql .= '
WHERE
				' . $sql_where ;
			}

			$sth = $this->_dbh->prepare( $sql ) ;

			foreach ( $sql_args as $arg_name => $arg_value ) {
				$sth->bindParam( $arg_name , $arg_value , \PDO::PARAM_STR ) ;
			}

			$sth->execute( ) ;
			$result = $sth->fetchAll( \PDO::FETCH_NUM ) ;
			$sth->closeCursor( ) ;

			return $result ;
		}

		/**
		* Преобразует входной "SQL" локальный формат в формат SQL для MySQL для подстановки в оператор "WHERE".
		* @param $query string - строка с запросом в с локальным SQL-форматом, как описано в исходной задаче:
		* Должна быть возможность составлять такие условия поиска как:
		* ((ID = 1000) ИЛИ (Страна != Россия))
		* ((Страна = Россия) И (Состояние пользователя != active) И (Граватар = Нет))
		* ((((Страна != Россия) ИЛИ (Состояние пользователя = active)) И (E-Mail = user@domain.com)) ИЛИ (Граватар = Есть))
		* @return array( $sql string , $sql_args array ):
		* первый аргумент - SQL, второй аргумент - хэш аргументов для запроса: "имя аргумента", "значение"
		*/
		public function sql_where( $query ) {
			$result = $condition = $operator = $field = $value = $neq = '' ;
			$close_brace = $is_value = false ;
			$braces = 0 ;
			$sql_args = array( ) ;

			foreach ( preg_split( '{(?<!^)(?!$)}us' , $query ) as $char ) {
				switch ( $char ) {
					case '(' : {
						$braces ++ ;

						if ( $close_brace ) {
							$condition = trim( $condition ) ;

							switch ( $condition ) {
								case '' : {
									break ;
								}
								case 'ИЛИ' : {
									$result .= ' OR ' ;

									break ;
								}
								case 'И' : {
									$result .= ' AND ' ;

									break ;
								}
								default : {
									throw new \Exception( 'Неизвестный оператор "' . $condition . '"' ) ;

									break ;
								}
							}

							$condition = '' ;
						}

						$close_brace = false ;

						break ;
					}
					case ')' : {
						$braces -- ;

						if ( ! empty( $field ) ) {
							$field = trim( $field ) ;
							$value = trim( $value ) ;

							if ( isset( $this->_fields[ $field ] ) ) {
								$field = $this->_fields[ $field ] ;
							} elseif ( ! isset( $this->_props[ $field ] ) ) {
								$field = $this->_props[ $field ] ;
							} else {
								throw new \Exception( 'Неизвестное поле "' . $field . '"' ) ;
							}

							$sql_arg_key = ':v' . sha1( $value ) ;
							$sql_args[ $sql_arg_key ] = $value ;

							$result .= "{$field} $operator {$sql_arg_key}" ;
						}

						$field = $value = $operator = '' ;
						$close_brace = true ;
						$is_value = false ;

						break ;
					}
					case '=' : {
						if ( $close_brace ) {
							break ;
						}

						$is_value = true ;

						if ( ! empty( $neq ) ) {
							$operator = '<>' ;
							$neq = '' ;

							continue 2 ;
						}

						$operator = $char ;

						continue 2 ;
					}
					case '!' : {
						if ( $close_brace ) {
							break ;
						}

						$is_value = true ;
						$neq = $char ;

						continue 2 ;
					}
					default: {
						if ( $close_brace ) {
							$condition .= $char ;

							continue 2 ;
						}

						if ( empty( $is_value ) ) {
							$field .= $char ;

							continue 2 ;
						}

						$value .= $char ;

						continue 2 ;
					}
				}

				$neq = '' ;
				$result .= $char ;
			}

			if ( $braces ) {
				throw new \Exception( 'Не соблюдён баланс скобок' ) ;
			}

			return array( $result , $sql_args ) ;
		}
	}