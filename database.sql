-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: test31
-- ------------------------------------------------------
-- Server version	5.5.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор',
  `email` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'E-mail',
  `password` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'Пароль',
  `role` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'Роль',
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата регистрации',
  `last_visit` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Крайний визит',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`) USING BTREE,
  KEY `email_password` (`email`,`password`) USING BTREE,
  KEY `role_reg_date` (`role`,`reg_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Пользователь';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'user@domain.com','12345','5','2018-01-28 10:05:10','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_about`
--

DROP TABLE IF EXISTS `users_about`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_about` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор',
  `user` int(10) unsigned NOT NULL COMMENT 'Пользователь',
  `item` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT 'Свойство',
  `value` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT 'Значение',
  `up_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Дата\\время обновления',
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `user_item_value` (`user`,`item`,`value`) USING BTREE,
  KEY `item` (`item`),
  CONSTRAINT `users_about_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Данные пользователя';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_about`
--

LOCK TABLES `users_about` WRITE;
/*!40000 ALTER TABLE `users_about` DISABLE KEYS */;
INSERT INTO `users_about` VALUES (1,1,'Страна','Древний Шумер','2018-01-28 10:34:47'),(2,1,'Состояние пользователя','active','2018-01-28 10:12:35'),(3,1,'Граватар','Есть','2018-01-28 10:12:35'),(5,1,'Имя','Пользователь','2018-01-28 10:13:19');
/*!40000 ALTER TABLE `users_about` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'test31'
--

--
-- Dumping routines for database 'test31'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-28 14:18:59
